FROM debian:bullseye-slim

ADD entrypoint.sh /

RUN apt update \
    && apt install -y \
        borgbackup \
        openssh-server \
    && apt clean

ENV SSHD_PORT 22

EXPOSE ${SSHD_PORT}

ENTRYPOINT ["/entrypoint.sh"]
